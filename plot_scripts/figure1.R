# Figure 1

# This script generates Figure 1b, 1c, and 1d.

library(dplyr)
library(ggplot2)
library(RColorBrewer)
library(reshape2)

setwd('~/repos/uorf-paper-2020')

###############
## Figure 1b ##
###############

all_utrs_maps <- data.frame(MAPS=5.576584e-03,
                            SE=5.183301e-04,
                            N=9.279620e+05)
all_utrs_maps <- all_utrs_maps %>%
  mutate(group = "all 5'UTR",
         effect = "all 5'UTR",
         X5. = 1.645*-SE+MAPS,
         X95. = 1.645*SE+MAPS)

cds_missense_plof_maps <- data.table::fread("plot_data/cds_missense_plof_maps.txt")


nc_maps <- data.table::fread('plot_data/maps_ncorf_predicted_consequence.txt')
nc_stop <- nc_maps %>%
  filter(group %in% c("dORF","lncRNA","pseudogene", "uORF-combined"),
         effect  == 'stop_gain') %>%
  mutate(effect = 'stop gained')

ctrl_maps <- data.table::fread('plot_data/maps_controls.txt')

ctrl_maps <- ctrl_maps %>%
  mutate(effect = ifelse(group == "OOF",
                         gsub("gained", "matched", effect),
                         effect))
ctrl_maps$effect[8] <- "stop>TAA matched"

stop_maps <- rbind(ctrl_maps,
                   nc_stop)

stop_maps <- stop_maps %>%
  filter(!effect %in% c("all positions",
                        "start maintained",
                        "start disrupted",
                        "start disrupted - short",
                        "start disrupted - long"))

other_stop_maps <- data.table::fread('plot_data/maps_uorf_other_stop.txt')
other_stop_maps$effect[4] <- "stop>TAA"

all_maps <- rbind(stop_maps, other_stop_maps)

nc_nonuorf_combined_maps <- data.table::fread("plot_Data/maps_nc_nonuorf_combined.txt")
nc_nonuorf_combined_maps <- nc_nonuorf_combined_maps %>%
  mutate(effect = gsub("stop gain", "stop gained", effect))
all_maps <- rbind(all_maps,
                  nc_nonuorf_combined_maps)

maps_all_uorf <- read.table("plot_data/maps_all_uorf.txt",
                            header = TRUE)
maps_all_uorf <- maps_all_uorf %>%
  mutate(effect = 'all SNVs')

all_maps <- rbind(all_maps, maps_all_uorf)
all_maps <- all_maps %>%
  mutate(group = gsub("-combined", "", group))


# load bootstrapped-CIs
load("RData/bootstraps/maps/nc_orf_newstop_bootstraps.RData")
load("RData/bootstraps/maps/uorf_cds_consequence_bootstraps.RData")
load('RData/bootstraps/maps/uorf_other_stop_bootstraps.RData')
load('RData/bootstraps/maps/oof_bootstraps.RData')
load('RData/bootstraps/maps/uorf_all_var_bootstrap.RData')
load("RData/bootstraps/maps/nc_nonuorf_combined_bootstraps.RData")
load("RData/bootstraps/maps/utr5_all_ctrls_bootstrap.RData")


# uptream termination codons
exp_cis <- list("dORF" = dorf_stop_gain_bootstrap,
                "pseudogene" = pseudogene_stop_gain_bootstrap,
                "lncRNA" = lncrna_stop_gain_bootstrap,
                "uORF" = uorf_stop_gain_bootstrap,
                "uORF.TAA" = uorf_other_stop_bootstraps$`5'UTR TAA` %>% unlist(),
                "uORF.stop>TAA" = uorf_other_stop_bootstraps$`5'UTR TGA|TAG > TAA` %>% unlist(),
                "ncORF" = nc_nonuorf_combined_bootstraps$stop_gain %>% unlist())

exp_cis <- lapply(exp_cis,
                  function(x) quantile(x, c(0.05, 0.95))) %>% do.call(rbind, .) %>%
  data.frame() %>%
  mutate(group = c("dORF","pseudogene","lncRNA",
                   "uORF",
                   "uORF",
                   "uORF",
                   "ncORF"),
         effect = c(rep("stop gained", 3),
                    "stop gained",
                    "TAA gained",
                    "stop>TAA",
                    "stop gained"))

ctrl_cis <- oof_bootstraps %>% 
  lapply(function(x) quantile(x, c(0.05, 0.95))) %>%
  do.call(rbind,.) %>% data.frame() %>%
  mutate(group = "OOF",
         effect = c("stop matched",
                    "TAA matched",
                    "stop>TAA matched",
                    "all uORF",
                    "start maintained",
                    "start disrupted",
                    "start disrupted - short",
                    "start disrupted - long"))


all_utr_cis <- utr5_all_ctrls_bootstrap %>%
  lapply(function(x) quantile(x, c(0.05, 0.95))) %>%
  do.call(rbind,.) %>%
  data.frame() %>%
  mutate(group = "5'UTR",
         effect = c("stop gained",
                    "TAA gained",
                    "stop>TAA"))

ctrl_cis <- rbind(ctrl_cis, all_utr_cis)

cis <- rbind(exp_cis, ctrl_cis)


uorf_all_var_cis <- uorf_all_var_bootstrap %>%
  quantile(c(0.05, 0.95)) %>%
  t() %>%
  as.data.frame() %>%
  mutate(group = 'uORF', effect = 'all SNVs')

colnames(uorf_all_var_cis) <- colnames(ctrl_cis)


cis <- rbind(cis, uorf_all_var_cis)


all_maps$effect[c(3, 6)] <- c("stop>TAA matched", "stop>TAA")


plot_df <- all_maps %>%
  left_join(., cis,
            by = c("group","effect"))

plot_df <- plot_df %>%
  rbind(., all_utrs_maps) # add 5'UTR MAPS scores for all variants

plot_df <- plot_df %>%
  mutate(group = gsub("OOF", "uORF-matched", group))

plot_df <- plot_df %>%
  mutate(group = factor(group,
                        levels = c("all 5'UTR","dORF","pseudogene",
                                   "lncRNA","ncORF",
                                   "5'UTR",
                                   "uORF-matched",
                                   "uORF")),
         effect = factor(effect, levels = c("all 5'UTR",
                                            "all SNVs",
                                            "stop matched",
                                            "TAA matched",
                                            "stop>TAA matched",
                                            "stop gained",
                                            "TAA gained",
                                            "stop>TAA")))


plot_df <- plot_df %>%
  filter(!is.na(effect),
         !group %in% c("dORF","lncRNA","pseudogene"))


plot_1b_df <- plot_df[c(4,7,8,
                        9,10,11,12),]

plot_1b_df <- plot_1b_df %>%
  mutate(names = paste0(effect, "\n(N=", N, ")"))

plot_1b_df$names[[1]] <- "5'UTR stop gained\n(N=63311)"
plot_1b_df$names[[5]] <- "ncORF stop gained\n(N=1935)"

plot_1b_df <- plot_1b_df %>%
  mutate(names = factor(names, levels = names[c(7,5,1,6,
                                                2,3,4)])) %>%
  mutate(two_color_group = as.character(c(1,2,2,2,1,1,1)))


# subset of MAPS scores plotted


fig_1b <- ggplot(plot_1b_df, aes(x = names, y = MAPS, color = two_color_group)) +
  geom_hline(yintercept = 0, lty = 2, color = 'grey') +
  geom_hline(yintercept = cds_missense_plof_maps$MAPS[[1]], lty = 2, color = 'gold') +
  geom_hline(yintercept = cds_missense_plof_maps$MAPS[[2]], lty = 2, color = 'purple') +
  geom_point(size = 2) + scale_color_manual(values = c("black","red")) +
  geom_errorbar(aes(ymin = X5., ymax = X95.,
                    width = 0),
                position = position_identity()) +
  xlab("") +
  ylim(c(-0.05, 0.15)) + 
  theme(axis.line = element_line(size = 1, color = 'black'),
        panel.background = element_rect((fill = 'white')),
        axis.text.x = element_text(angle = 30, hjust = 1, size = 8),
        axis.title.y = element_text(),
        plot.margin=unit(c(0,0,0,0.1),"cm"),
        legend.position = 'none') +
  annotate("text", x = 0.8, y = 0.045, label = "missense", color = 'orange', size = 2.75) +
  annotate("text", x = 0.85, y = 0.005, label = "synonymous", color = 'grey', size = 2.75) +
  annotate("text", x = 0.65, y = 0.135, label = "pLOF", color = 'purple', size = 2.75) +
  geom_vline(xintercept = c(3.5), lty = 1, color = 'grey') +
  annotate("text", x = c(2,5.5),
           y = -0.045,
           label = c("i. non-uORFs",
                     "ii. uORFs"),
           size = 2.75)


pdf(file = 'plots/1b_fix.pdf',
    width = 5.25, height = 3)
fig_1b
dev.off()

pdf(file = 'plots/1b_resize.pdf',
    width = 4.75, height = 3)
fig_1b
dev.off()



# make supplemental figure

#suppl_uorf_utc_ss_maps <- plot_df[c(4,1,7,
#                                    5,2,8,
#                                    6,3,9),]

#suppl_uorf_utc_ss_maps <- suppl_uorf_utc_ss_maps %>%
#  mutate(names = paste0(effect, "\n(N=", N, ")")) %>%
#  mutate(names = factor(names, levels = names)) %>%
#  mutate(variants = as.character(rep(c("All 5'UTR","uORF-matched","uORF"), 3)))


suppl_uorf_utc_ss_maps <- plot_df[c(1,7,
                                    2,8,
                                    3,9),]

suppl_uorf_utc_ss_maps <- suppl_uorf_utc_ss_maps %>%
  mutate(names = paste0(effect, "\n(N=", N, ")")) %>%
  mutate(names = factor(names, levels = names)) %>%
  mutate(variants = as.character(rep(c("uORF-matched","uORF"), 3)))

suppl_uorf_maps <- ggplot(suppl_uorf_utc_ss_maps, aes(x = names, y = MAPS, color = variants)) +
  geom_hline(yintercept = 0, lty = 2, color = 'grey') +
  geom_hline(yintercept = cds_missense_plof_maps$MAPS[[1]], lty = 2, color = 'gold') +
  geom_hline(yintercept = cds_missense_plof_maps$MAPS[[2]], lty = 2, color = 'purple') +
  geom_point(size = 2) + scale_color_manual(values = c("red","black")) +
  geom_errorbar(aes(ymin = X5., ymax = X95.,
                    width = 0),
                position = position_identity()) +
  xlab("") +
  ylim(c(-0.05, 0.15)) + 
  theme(axis.line = element_line(size = 1, color = 'black'),
        panel.background = element_rect((fill = 'white')),
        axis.text.x = element_text(angle = 45, hjust = 1, size = 8),
        axis.title.y = element_text(),
        plot.margin=unit(c(0,0,0,0.1),"cm"),
        legend.position = 'bottom') +
  annotate("text", x = 0.8, y = 0.045, label = "missense", color = 'orange', size = 2.75) +
  annotate("text", x = 0.85, y = 0.005, label = "synonymous", color = 'grey', size = 2.75) +
  annotate("text", x = 0.65, y = 0.135, label = "pLOF", color = 'purple', size = 2.75)


pdf(file = 'plots/supplX.pdf',
    width = 5, height = 4)
suppl_uorf_maps
dev.off()


###############
## Figure 1C ##
###############

phylop <- data.table::fread("proc_data/phylop_scores.txt")

# load bootstrapped CIs
load('RData/bootstraps/new_stop_uorfs_bootstraps.RData')
load('RData/bootstraps/all_utr_new_distmatched_bootstraps.RData')
load('RData/bootstraps/uorf_nontrans_new_bootstraps.RData')
load('RData/bootstraps/new_stop_oof_bootstraps.RData')

phylop_stop <- phylop %>%
  filter(group == 'STOP')

stop_cis <- list(new_stop_uorfs_bootstraps,
                 all_utr_new_distmatched_bootstraps,
                 uorf_nontrans_new_bootstraps,
                 new_stop_oof_bootstraps)
stop_cis_effects <- lapply(stop_cis, function(x) names(x)) %>%
  unlist() %>% toupper() %>%
  gsub("COMBINED","ALL",.)

stop_cis <- lapply(stop_cis,
                   function(x) do.call(rbind,
                                       lapply(x,
                                              function(y) quantile(y, c(0.05, 0.95))))) %>%
  do.call(rbind, .) %>% data.frame() %>%
  mutate(group = rep("STOP", 20),
         feature = c(rep("uORF", 5),
                     rep("UTR", 5),
                     rep("uORF-UTR", 5),
                     rep("OOF", 5)),
         effect = c(stop_cis_effects))

phylop_stop <- phylop_stop %>%
  left_join(., stop_cis, by = c("group", "feature", "effect"))

phylop_stop$effect <- factor(phylop_stop$effect,
                             levels = c("ALL",
                                        "TGA",
                                        "TAG",
                                        "TAG|TAA",
                                        "TAA"))

stop_plot <- phylop_stop %>%
  filter(effect != 'TAG|TAA')
stop_plot$effect <- factor(stop_plot$effect,
                           levels = c("ALL",
                                      "TGA",
                                      "TAG",
                                      "TAA"))
stop_plot$feature <- factor(stop_plot$feature,
                            levels = c("uORF","OOF","uORF-UTR",
                                       "UTR"))

stop_plot <- stop_plot[c(16:1),] # reorder to help with plotting points

stop_phylop_plot <- ggplot(stop_plot, aes(x=effect, y=value, group=feature, color=feature)) +
  geom_point(size = 2) +
  geom_errorbar(aes(ymin=X5.,
                    ymax=X95.,
                    width = 0)) +
  theme(panel.background = element_blank(),
        axis.text.x = element_text(angle = 60, hjust = 1),
        axis.line.x = element_line(color = "black", size = 1),
        axis.line.y = element_line(color = "black", size = 1)) +
  ylim(c(0, 0.54)) +
  geom_hline(yintercept = 0.09792254,
             lty = 2,
             color = 'grey') +
  geom_hline(yintercept = 0.501860406293776, lty = 2,
             color = 'purple') +
  xlab("Trinucleotide Created") +
  ylab("Proportion of conserved bases\n(phyloP > 2)") +
  scale_color_manual(values = c("coral", "aquamarine3","black","darkgrey")) +
  geom_vline(xintercept = 1.5, lty = 1, color = 'grey') +
  annotate("text", x = 3, y = 0.01, label = "Increasing Stop Strength",
           size = 2.75) +
  annotate("text", x = c(0.75, 0.75), y = c(0.12, 0.52), label = c("All UTRs", "CDS"),
           color = c("grey", "purple"), size = 2.75)

pdf(file = 'plots/1c.pdf',
    height = 2.4,
    width = 4)
stop_phylop_plot
dev.off()

png(file = 'plots/1c.png',
    height = 3.5,
    width = 5.25,
    units = 'in',
    res = 300)
stop_phylop_plot
dev.off()



###############
## Figure 1D ##
###############

load('RData/stop_plot_data.RData')


permutation_taa <- quantile(random_stops_notx_prop[,'TAA'],
                            c(0.025,0.975))
permutation_tag <- quantile(random_stops_notx_prop[, 'TAG'],
                            c(0.025, 0.975))
permutation_tga <- quantile(random_stops_notx_prop[, 'TGA'],
                            c(0.025, 0.975))

permutation_all_taa <- quantile(utrs_stop_tri_all_permute[,'TAA'],
                                c(0.025,0.975))
permutation_all_tag <- quantile(utrs_stop_tri_all_permute[, 'TAG'],
                                c(0.025, 0.975))
permutation_all_tga <- quantile(utrs_stop_tri_all_permute[, 'TGA'],
                                c(0.025, 0.975))

permutation_uorf_taa <- quantile(tx_uorf_bootstrap_df_permute[,'TAA'],
                                 c(0.025,0.975))
permutation_uorf_tag <- quantile(tx_uorf_bootstrap_df_permute[, 'TAG'],
                                 c(0.025, 0.975))
permutation_uorf_tga <- quantile(tx_uorf_bootstrap_df_permute[, 'TGA'],
                                 c(0.025, 0.975))

stop_df <- do.call(rbind, list(colMeans(utrs_stop_tri_all_permute),
                               colMeans(random_stops_notx_prop),
                               tx_uorf_canonical_dist_prop)) %>%
  melt()

stop_df$Var2 <- factor(stop_df$Var2, levels = c("TGA", "TAG", "TAA"))

stop_df$Context <- factor(rep(c("All 5'UTRs", "5'UTRs w/ uORF","uORF"), 3),
                          levels = c("All 5'UTRs", "5'UTRs w/ uORF","uORF"))


stop_df$lci <- c(permutation_all_taa['2.5%'],
                 permutation_taa['2.5%'],
                 permutation_uorf_taa['2.5%'],
                 permutation_all_tag['2.5%'],
                 permutation_tag['2.5%'],
                 permutation_uorf_tag['2.5%'],
                 permutation_all_tga['2.5%'],
                 permutation_tga['2.5%'],
                 permutation_uorf_tga['2.5%'])

stop_df$uci <- c(permutation_all_taa['97.5%'],
                 permutation_taa['97.5%'],
                 permutation_uorf_taa['97.5%'],
                 permutation_all_tag['97.5%'],
                 permutation_tag['97.5%'],
                 permutation_uorf_tag['97.5%'],
                 permutation_all_tga['97.5%'],
                 permutation_tga['97.5%'],
                 permutation_uorf_tga['97.5%'])

write.table(stop_df,
            file = "plot_data/stop_distribution.txt",
            quote = FALSE,
            sep = '\t',
            row.names = FALSE)

dodge <- position_dodge((width=0.9))


fig_1d <- ggplot(stop_df, aes(x=Var2, y=value, fill=Context)) +
  geom_hline(yintercept=c(0.1,0.2,0.3,0.4,0.5),
             col='lightgrey', lty =2) +
  geom_col(position=dodge) +
  theme(panel.background = element_blank()) +
  xlab("Stop Codon") +
  ylab("Proportion") +
  geom_errorbar(aes(ymin=lci,ymax=uci),
                width=0.25,
                position=dodge) +
  scale_fill_manual(values=c("darkgrey","aquamarine3","coral")) +
  ylim(c(0,0.55))


pdf(file = 'plots/1d.pdf',
    height = 2.5, width = 3.5)
fig_1d
dev.off()

png(file = 'plots/1d.pdf.png',
    height = 2.5, width = 3.5, res = 300, units = 'in')
fig_1d
dev.off()



# p values:
# is TAA depleted in uORFs?
length(which(tx_uorf_bootstrap_df_permute[,'TAA'] > utrs_stop_tri_all_permute[,'TAA']))
length(which(tx_uorf_bootstrap_df_permute[,'TAA'] > random_stops_notx_prop[,'TAA']))

# prop uORF TAA compared to surrounding UTR
#1-(0.1966084/0.3548134)


