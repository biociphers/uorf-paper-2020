# This script contains functions used in phyloP analyses


phylop_prop <- function(phylo_df, threshold = 2) {
  
  # returns proportion of genomic positions wihtin phylo_df
  # with a phyloP value > threshold
  
  
  result <- table(phylo_df$phylop > threshold)['TRUE']/sum(table(phylo_df$phylop > threshold))
  
  return(result)
  
}

bootstrap_stops <- function(tx_stop, uorf = FALSE) {
  
  if (uorf == TRUE) {
    
    result <- tx_stop[sample(nrow(tx_stop), replace = TRUE),] %>%
      select(codons) %>%
      table()
    
    return(result)
    
  } else {
    
    result <- tx_stop[sample(nrow(tx_stop)),] %>%
      filter(!duplicated(.id)) %>%
      select(trimer) %>%
      table()
    
    return(result)
    
  }
  
}

find_potential_stop_positions <- function(tiled_exons, translated = TRUE) {
  
  if (translated) {
    
    potential_stop1 <- tiled_exons %>%
      filter(position == 1,
             grepl(pattern = '[AGC]AA|[AGC]AG|[AGC]GA', codons)) %>%
      mutate(REF = ifelse(strand == '+',
                          substr(codons, 1, 1),
                          chartr('ATGC','TACG', substr(codons, 1, 1))),
             dna_stop_creating_variant = ifelse(strand == '+',
                                                'T',
                                                'A'),
             new_stop_codon = paste0('T', substr(codons, 2, 3)))
    
    potential_stop2 <- tiled_exons %>%
      filter(position == 2,
             grepl(pattern = 'T[CT]A|T[CTG]G', codons)) %>%
      mutate(REF = ifelse(strand == '+',
                          substr(codons, 2, 2),
                          chartr('ATGC','TACG', substr(codons, 2, 2))),
             dna_stop_creating_variant = ifelse(codons %in% c("TCG","TGG","TTG"),
                                                ifelse(strand == '+',
                                                       'A',
                                                       'T'),
                                                ifelse(strand == '+',
                                                       'A|G',
                                                       'T|C')),
             new_stop_codon = ifelse(substr(codons, 3, 3) == 'A',
                                     'TAG|TAA',
                                     'TAG'))
    
    potential_stop3 <- tiled_exons %>%
      filter(position == 3,
             grepl(pattern = 'TA[CT]|TG[CTG]', codons)) %>%
      mutate(REF = ifelse(strand == '+',
                          substr(codons, 3,3),
                          chartr('ATGC','TACG', substr(codons, 3,3))),
             dna_stop_creating_variant = ifelse(codons %in% c("TAT","TAC"),
                                                ifelse(strand == '+',
                                                       'A|G',
                                                       'T|C'),
                                                ifelse(strand == '+',
                                                       'A',
                                                       'T')),
             new_stop_codon = ifelse(substr(codons, 1, 2) == 'TG',
                                     'TGA',
                                     'TAG|TAA'))
    
  } else {
    
    
    potential_stop1 <- tiled_exons %>%
      filter(grepl(pattern = '[AGC]AA|[AGC]AG|[AGC]GA',
                   substr(RNA_heptamer, 4, 6))) %>%
      mutate(REF = ifelse(strand == '+',
                          substr(RNA_heptamer, 4, 4),
                          chartr('ATGC','TACG', substr(RNA_heptamer, 4, 4))),
             dna_stop_creating_variant = ifelse(strand == '+',
                                                'T',
                                                'A'),
             new_stop_codon = paste0('T', substr(RNA_heptamer, 5, 6)))
    
    potential_stop2 <- tiled_exons %>%
      filter(grepl(pattern = 'T[CT]A|T[CTG]G',
                   substr(RNA_heptamer, 3, 5))) %>%
      mutate(REF = ifelse(strand == '+',
                          substr(RNA_heptamer, 4, 4),
                          chartr('ATGC','TACG', substr(RNA_heptamer, 4, 4))),
             dna_stop_creating_variant = ifelse(substr(RNA_heptamer, 3, 5) %in% c("TCG","TGG","TTG"),
                                                ifelse(strand == '+',
                                                       'A',
                                                       'T'),
                                                ifelse(strand == '+',
                                                       'A|G',
                                                       'T|C')),
             new_stop_codon = ifelse(substr(RNA_heptamer, 5, 5) == 'A',
                                     'TAG|TAA',
                                     'TAG'))
    
    potential_stop3 <- tiled_exons %>%
      filter(grepl(pattern=  'TA[CT]|TG[CTG]',
                   substr(RNA_heptamer, 2, 4))) %>%
      mutate(REF = ifelse(strand == '+',
                          substr(RNA_heptamer, 4, 4),
                          chartr('ATGC','TACG', substr(RNA_heptamer, 4,4))),
             dna_stop_creating_variant = ifelse(substr(RNA_heptamer, 2, 4) %in% c("TAT","TAC"),
                                                ifelse(strand == '+',
                                                       'A|G',
                                                       'T|C'),
                                                ifelse(strand == '+',
                                                       'A',
                                                       'T')),
             new_stop_codon = ifelse(substr(RNA_heptamer, 2, 3) == 'TG',
                                     'TGA',
                                     'TAG|TAA'))
  }
  
  potential_stop1 <- potential_stop1 %>%
    mutate(varpos = 1)
  potential_stop2 <- potential_stop2 %>%
    mutate(varpos = 2)
  potential_stop3 <- potential_stop3 %>%
    mutate(varpos = 3)
  
  newstops <- rbind(potential_stop1,
                    potential_stop2)
  
  newstops <- rbind(newstops,
                    potential_stop3)
  
  return(newstops)
  
}

split_stop_positions <- function(new_stops) {
  
  taa <- new_stops  %>%
    filter(!duplicated(position_id),
           new_stop_codon == 'TAA')
  
  tag <- new_stops %>%
    filter(!duplicated(position_id),
           new_stop_codon == 'TAG')
  
  tga <- new_stops %>%
    filter(!duplicated(position_id),
           new_stop_codon == 'TGA')
  
  tag_taa <- new_stops %>%
    filter(!duplicated(position_id),
           new_stop_codon == 'TAG|TAA')
  
  return(list('taa'=taa,
              'tag'=tag,
              'tga'=tga,
              'tag|taa'=tag_taa))
  
}

bootstrap_phylop <- function(phylo_df, n=10000) {
  
  phylop_scores <- phylo_df$phylop
  n_positions <- length(phylop_scores)
  
  res_vec <- vector("list",length=n)
  
  sampling <- lapply(res_vec, function(x) sample(n_positions, replace = TRUE))
  
  phylop <- sapply(sampling, function(x) (length(which(phylop_scores[x] > 2))/n_positions))
  
  return(phylop)
  
}

sample_distance_variants <- function(variants, n) {
  
  sampled <- variants[sample(c(1:dim(variants)[[1]]), size = n,
                             replace = FALSE), ]
  
  return(sampled)
  
}

get_matched_distance_variants <- function(match_set, controls) {
  
  controls <- split(controls, controls$binned_distance)
  
  # remove variants out of position
  match_set <- match_set[names(match_set) >= 0]
  
  matched_variants <- lapply(names(match_set),
                             function(x) sample_distance_variants(controls[x][[1]],
                                                                  match_set[x]))
  matched_variants <- as.data.frame(do.call(rbind, matched_variants))
  
  return(matched_variants)
  
}
