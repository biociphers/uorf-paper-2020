**Disrupting upstream translation in mRNAs is associated with loss-of-function in human disease**

This repo contains data processing and analysis scripts to reproduce the analyses of our manuscript.


---

## Additional data needed

To reproduce this analysis genome-wide methylation estimates and bootstrapped MAPS/phyloP scores can be obtained [here](https:////drive.google.com/file/d/1fPxgv5WP4LSJ4Unm2T_OMyuWoOrpFnt6/view?usp=sharing).

The bootstrapped MAPS/phyloP scores should be placed in a directory titled "RData" once downloaded. Alternatively these bootstrapped scores can be generated using the bootstrap_phylop and bootstrap_maps functions in the utility scripts directory. It is recommended that the bootstrapping functions be run on a machine with >32 GB memory.


---

## Generating the MAPS model

To generate the MAPS model, run the "make_maps_model.R" script in "Notebooks".

This will generate an RData object saved in "RData" which will be loaded to calculate MAPS scores for sets of variants.

---

## Reproducing the analyses

To reproduce the data needed to generate Figures 1-3 in the manuscript run the following scripts in order with R from the Notebooks directory:

1. missense_plof_maps.R - this script calculates MAPS scores for missense and predicted loss-of-function (pLOF) variants from gnomAD v3 using the maps model generated
2. tile_map_orfs.R - this script will create an RData object where genomic positions of each mapped ORF from the Ji et al. 2015 ribosome profiling study are annotated based on their predicted reading frame, and position within the ORF. This RData object is used for variant annotation in downstream analyses with MAPS and phyloP scores.
3. utr_annotation.R - this script will generate required UTR annotations for MAPS and phyloP analysis downstream
4. maps_analysis_mutation_matched.R - this script calculates MAPS scores for different sets of uORF and non-uORF variants needed to generate the data in Figure 1 of the manuscript
5. uorf_stop_distribution.R - this script generates a tile-mapped RData object for all UTR sequences, and also determines the distribution of stop codons used in translated uORF sequences for Figure 1c. The tile-mapped UTR sequence object will be needed to generate phyloP scores for non-uORF and uORF sequences.
6. phyloP_analysis.R - this script performs the phyloP analysis used in Figures 1d and Figure 2 for uORF stop-introducing and start-disrupting genomic positions.
7. optimality_maps.R - performs MAPS analysis based on uORF and non-uORF variants split by predicted impact of SNVs in gnomAD v3 release on codon optimality. Optimality scores were derived from Wu et al. eLife 2019.
8. generate_bootstraps_maps.R - generates bootstrapped confidence intervals for MAPS scores for plotting and statistical analysis (note: the bootstrapping procedure is memory intensive so it is recommended to run these on a machine with at least XX GB of memory)
9. generate_bootstraps_phyloP.R - generates bootstrapped confidence intervals for phyloP scores for plotting and statistical analysis


---

## Reproducing the figures

To reproduce figures in the manuscript run scripts in the "plot_scripts" directory in order. This will generate a new directory called "plots" in which each figure panel from Figs. 1-3 will be reproduced and deposited.
